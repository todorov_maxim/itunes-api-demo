import { iTunesUrl } from 'constants/itunes';

export const getItunesItems = (term, type) => {
  const query =
    iTunesUrl +
    '/search?term=' +
    term.replace(/ /g, '+') +
    '&limit=5&entity=' +
    type;

  return fetch(query, {
    method: 'GET',
    headers: {
      'Access-Control-Allow-Origin': iTunesUrl,
    },
  }).then(response => {
    return response.json();
  });
};
