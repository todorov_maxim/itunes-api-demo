import React from 'react';
import MediaFinder from 'components/media-finder';

function App() {
  return (
    <div className="App">
      <h1 className='title'>Welcome to our iTunes media finder</h1>
      <MediaFinder />
    </div>
  );
}

export default App;
