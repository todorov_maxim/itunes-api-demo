import React, { useState } from 'react';
import Search from 'components/media-finder/components/search';
import Items from 'components/media-finder/components/items';

const MediaFinder = () => {
  const [items, setItems] = useState([]);

  return (
    <div>
      <Search setItems={setItems} />
      <Items items={items} />
    </div>
  );
};

export default MediaFinder;
