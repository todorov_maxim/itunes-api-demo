import React, { useState } from 'react';
import TextField from '@material-ui/core/TextField';
import MenuItem from '@material-ui/core/MenuItem';
import Button from '@material-ui/core/Button';
import { getItems } from 'action/media-finder';

const mediaTypes = [
  'movie',
  'podcast',
  'musicVideo',
  'audiobook',
  'shortFilm',
  'tvShow',
  'software',
  'ebook',
  'all',
];

const Search = ({ setItems }) => {
  const [mediaName, setMediaName] = useState('');
  const [mediaType, setMediaType] = useState('all');

  const handleChangeName = ({ target }) => {
    setMediaName(target.value);
  };

  const handleChangeType = ({ target }) => {
    setMediaType(target.value);
  };

  const handleSubmit = e => {
    e.preventDefault();
    getItems(mediaName, mediaType).then(items => {
      if (items.length === 0) {
        alert('Nothing found');
      }
      setItems(items);
    });
  };

  return (
    <form className="search-form" onSubmit={handleSubmit}>
      <TextField
        value={mediaName}
        onChange={handleChangeName}
        id="media-name"
        label="Media name"
      />
      <TextField
        id="media-type"
        select
        label="Media type"
        value={mediaType}
        onChange={handleChangeType}
        helperText="Please select media type"
      >
        {mediaTypes.map(type => (
          <MenuItem key={type} value={type}>
            {type}
          </MenuItem>
        ))}
      </TextField>
      <Button variant="contained" color="primary" type="submit">
        Search
      </Button>
    </form>
  );
};

export default Search;
