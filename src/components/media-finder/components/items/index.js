import React from 'react';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import ListItemAvatar from '@material-ui/core/ListItemAvatar';
import Avatar from '@material-ui/core/Avatar';
import AvatarPng from 'components/media-finder/assets/avatar.png';

const ListItemLink = props => {
  return <ListItem button component="a" {...props} />;
};

const Items = ({ items }) => {
  const mediaItems = items.map((item, key) => (
    <ListItemLink key={key} href={item.previewUrl || '#'}>
      <ListItemAvatar>
        <Avatar src={item.artworkUrl100 || AvatarPng} />
      </ListItemAvatar>
      <ListItemText primary={'Check it'} />
    </ListItemLink>
  ));

  return (
    <div className="list">
      <List>{mediaItems}</List>
    </div>
  );
};

export default Items;
