import { getItunesItems } from 'api/itunes';

export const getItems = (name, type) => {
  type = type === 'all' ? ' ' : type;
  return getItunesItems(name, type).then(
    res => {
      if (res.results) {
        return res.results;
      }
      return alert('Error');
    },
    err => {
      console.log('Error', err);
    },
  );
};
