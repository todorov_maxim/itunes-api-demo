## iTunes Api Demo

To install all dependencies
### `npm i`
To serve project
### `npm run start`
To build project
### `npm run build`
To format code
### `npm run format`
##

For using Prettier in Webstorm read: 
[Click here](https://medium.com/@jm90mm/adding-prettier-to-webstorm-a218eeec04d2)


### Improvements 
We can easily add Redux to our architecture to scale up our app.
Also it will be good to spend more time on handling errors.